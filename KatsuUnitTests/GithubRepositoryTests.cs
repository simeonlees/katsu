﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Katsu;
using Katsu.vcsRepository;

namespace KatsuUnitTests
{
    [TestClass]
    public class GithubRepositoryTests
    {

        /// <summary>
        /// Checks that this method returns true with valid credentials, meaning that a repository has been created.
        /// </summary>
        [TestMethod]
        public void initialiseRepository_withValidCredentials_returnsTrue()
        {
            // Arrange
            GithubRepository repository = new GithubRepository();
            string username = "simlees";
            string repositoryName = "leedsformula";
            bool result;

            // Act
            result = repository.initialiseRepository(username, repositoryName);

            // Assert
            Assert.IsTrue(result);

        }

        /// <summary>
        /// Asserts that no repository is initialised with invalid credentials
        /// </summary>
        [TestMethod]
        public void initialiseRepository_withInvalidCredentials_returnsFalse()
        {
            // Arrange
            GithubRepository repository = new GithubRepository();
            string username = "sdfljsadfslfkj";
            string repositoryName = "sdlkj494lsfslkj";
            bool result;

            // Act
            result = repository.initialiseRepository(username, repositoryName);

            // Assert
            Assert.IsFalse(result);

        }

        /// <summary>
        /// Asserts that data is returned from the Git URL - requires internet connection
        /// </summary>
        [TestMethod]
        public void getGithubData_returnsData()
        {
            // Arrange
            GithubRepository repository = new GithubRepository();
            string url = "https://api.github.com/repos/simlees/leedsformula/git/trees/master?recursive=true";
            string result;

            // Act
            result = repository.getGithubData(url);

            // Assert
            Assert.IsNotNull(result);

        }

        /// <summary>
        /// Checks that directory structure is returned when requested
        /// </summary>
        [TestMethod]
        public void getDirectory_returnsDirectory()
        {
            // Arrange
            GithubRepository repository = new GithubRepository();
            repository.initialiseRepository("simlees", "leedsformula");
            List<VcsFile> result;

            // Act
            result = repository.getDirectory();

            // Assert
            Assert.IsNotNull(result);

        }
    }
}
