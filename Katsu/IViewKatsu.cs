﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu
{
    public interface IViewKatsu
    {
        void setStatus(string status);
        void resetStatus();
        void init(Project project);
        void init();
        void refresh();
        void selectBug(int bugId);
        int getProjectId();
        int getUserId();
        bool isInitialized();
    }
}
