﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Katsu
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class KatsuDBEntities : DbContext
    {
        public KatsuDBEntities()
            : base("name=KatsuDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<File> File { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<ProjectUserRole> ProjectUserRole { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Bug> Bug { get; set; }
        public virtual DbSet<Action> Action { get; set; }
    }
}
