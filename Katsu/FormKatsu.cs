﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Katsu.newProject;
using Katsu.openProject;
using Katsu.bug;

namespace Katsu
{
    public partial class FormKatsu : Form, IViewKatsu
    {
        private User _user;
        private Project _project;
        private List<Bug> _bugs;
        private List<Bug> _fileBugs;
        private bool _isInitialized;
        private DataTable _bugActionsDataTable;
        private Bug _selectedBug;
        private PresenterKatsu _presenterKatsu;

        public FormKatsu(User user)
        {
            InitializeComponent();
            _user = user;
            _presenterKatsu = new PresenterKatsu(this);
            _isInitialized = false;
            resetStatus();
        }

        public void init()
        {
            clearBugDetails();
            populateTreeView();
            populateBugTable();
            _isInitialized = true;

            if(dataGridViewBugs.RowCount > 1)
            {
                populateBugDetails((Bug)dataGridViewBugs.CurrentRow.DataBoundItem);
            }
        }

        public void refresh()
        {
            populateBugTable();    
        }

        public void selectBug(int bugId)
        {
            foreach (DataGridViewRow row in dataGridViewBugs.Rows)
            {
                if (row.Cells[0].Value.ToString() == bugId.ToString())
                {
                    dataGridViewBugs.CurrentCell = dataGridViewBugs.Rows[row.Index].Cells[0];
                    dataGridViewBugs.BeginEdit(true);
                    populateBugDetails((Bug)dataGridViewBugs.CurrentRow.DataBoundItem);
                    break;
                }
            }

            //dataGridViewBugs.CurrentCell = dataGridViewBugs.Rows[1].Cells[0];

        }

        public void init(Project project)
        {
            _project = project;
            init();
        }

        public bool isInitialized()
        {
            return _isInitialized;
        }

        public int getUserId()
        {
            return _user.Id;
        }

        private void populateTreeView()
        {
            treeViewProjectDirectory.Nodes.Clear();
            TreeNode root = new TreeNode(_project.VcsRepository);
            treeViewProjectDirectory.Nodes.Add(root);

            List<File> buffer = _presenterKatsu.getAllProjectFiles(_project);
                       
            List<File> cBuffer;
            int level = 0;
            TreeNode parent = root;

            while (buffer.Count > 0)
            {
                cBuffer = buffer.Where(f => f.FilePath.Count(x => x == '/') == level).OrderBy(f => f.FilePath).OrderBy(f => !f.isDirectory).ToList(); // Gets all files with filepath containing x no. of "/" and sorts alphabetically
                foreach (File file in cBuffer)
                {
                    parent = root;

                    for (int i = 0; i < level; i++)
                    {
                        string dir = file.FilePath.Split('/')[i];
                        TreeNode[] result = parent.Nodes.Cast<TreeNode>()
                                                        .Where(n => n.Text == dir)
                                                        .ToArray();
                        if (result.Length > 0) { parent = result[0]; }
                    }
                    
                    TreeNode node = new TreeNode(file.Name);
                    //node.Tag = file.isDirectory ? null : file.Url;
                    node.Tag = file;
                    node.ForeColor = file.isDirectory ? Color.Black : Color.DarkGreen;
                    parent.Nodes.Add(node);
                }

                buffer = buffer.Except(cBuffer).ToList(); // Removes processed files from buffer
                level++;
            }
            root.Expand();
        }

        private void populateBugTable()
        {
            _bugs = _presenterKatsu.getAllProjectBugs(_project);
            dataGridViewBugs.DataSource = _bugs;  
        }

        private void populateBugDetails(Bug bug)
        {
            _selectedBug = bug;
            textBoxId.Text = _selectedBug.Id.ToString();
            textBoxStatus.Text = _selectedBug.Status;
            textBoxPriority.Text = _selectedBug.Priority;
            textBoxSummary.Text = _selectedBug.Summary;

            _bugActionsDataTable = _presenterKatsu.getAllBugActions(_selectedBug);
            dataGridViewActions.DataSource = _bugActionsDataTable;
        }

        private void clearBugDetails()
        {
            _selectedBug = null;
            _bugActionsDataTable = null;
            dataGridViewActions.DataSource = _bugActionsDataTable;
            textBoxId.Clear();
            textBoxStatus.Clear();
            textBoxPriority.Clear();
            textBoxSummary.Clear();
        }

        private void populateFileBugTable(File file)
        {
            _fileBugs = _presenterKatsu.getAllFileBugs(file);
            dataGridViewFileBugs.DataSource = _fileBugs;
        }

        private void newProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNewProject newProjectWizard = new FormNewProject(this, _user);
            newProjectWizard.Show();

        }

        private void openProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormOpenProject openProjectForm = new FormOpenProject(this, _user);
            openProjectForm.Show();
        }

        public void setStatus(string message)
        {
            toolStripStatusLabel.Text = message;
        }

        public void resetStatus()
        {
            toolStripStatusLabel.Text = "Ready";
        }

        public int getProjectId()
        {
            return _project.Id;
        }

        private void treeViewProjectDirectory_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                File file = (File)e.Node.Tag;
                if (file.Url != null)
                {
                    string rawText = _presenterKatsu.getRawFile(file.Url);
                    fastColoredTextBoxCodeWindow.Text = rawText;
                }
                populateFileBugTable(file);
            }
        }

        private void reportBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_isInitialized)
            {
                return;
            }
            FormBug formBug = new FormBug(this);
            formBug.Show();
        }

        private void dataGridViewBugs_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if(e.RowIndex != -1)
            {
                int rowIndex = e.RowIndex;

                FormBug formBug = new FormBug(this, (Bug)dataGridViewBugs.Rows[rowIndex].DataBoundItem);
                formBug.Show();
            }
            
        }

        private void dataGridViewFileBugs_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                int rowIndex = e.RowIndex;

                FormBug formBug = new FormBug(this, (Bug)dataGridViewBugs.Rows[rowIndex].DataBoundItem);
                formBug.Show();
            }
                
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridViewBugs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                populateBugDetails((Bug)dataGridViewBugs.Rows[e.RowIndex].DataBoundItem);
            }            
        }

    }
}
