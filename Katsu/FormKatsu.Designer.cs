﻿namespace Katsu
{
    partial class FormKatsu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormKatsu));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportBugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.tabDashboard = new System.Windows.Forms.TabPage();
            this.splitContainerDashboard = new System.Windows.Forms.SplitContainer();
            this.dataGridViewBugs = new System.Windows.Forms.DataGridView();
            this.dataGridViewActions = new System.Windows.Forms.DataGridView();
            this.panelBugIdentifier = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSummary = new System.Windows.Forms.TextBox();
            this.textBoxPriority = new System.Windows.Forms.TextBox();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.tabProjectBrowser = new System.Windows.Forms.TabPage();
            this.dataGridViewFileBugs = new System.Windows.Forms.DataGridView();
            this.fastColoredTextBoxCodeWindow = new FastColoredTextBoxNS.FastColoredTextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.treeViewProjectDirectory = new System.Windows.Forms.TreeView();
            this.katsuDB2DataSet = new Katsu.KatsuDB2DataSet();
            this.katsuDB2DataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.tabDashboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerDashboard)).BeginInit();
            this.splitContainerDashboard.Panel1.SuspendLayout();
            this.splitContainerDashboard.Panel2.SuspendLayout();
            this.splitContainerDashboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBugs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewActions)).BeginInit();
            this.panelBugIdentifier.SuspendLayout();
            this.tabProjectBrowser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFileBugs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxCodeWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.katsuDB2DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.katsuDB2DataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.bugsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(955, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.openProjectToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newProjectToolStripMenuItem
            // 
            this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
            this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.newProjectToolStripMenuItem.Text = "New Project";
            this.newProjectToolStripMenuItem.Click += new System.EventHandler(this.newProjectToolStripMenuItem_Click);
            // 
            // openProjectToolStripMenuItem
            // 
            this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
            this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.openProjectToolStripMenuItem.Text = "Open Project";
            this.openProjectToolStripMenuItem.Click += new System.EventHandler(this.openProjectToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // bugsToolStripMenuItem
            // 
            this.bugsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportBugToolStripMenuItem});
            this.bugsToolStripMenuItem.Name = "bugsToolStripMenuItem";
            this.bugsToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.bugsToolStripMenuItem.Text = "Bugs";
            // 
            // reportBugToolStripMenuItem
            // 
            this.reportBugToolStripMenuItem.Name = "reportBugToolStripMenuItem";
            this.reportBugToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.reportBugToolStripMenuItem.Text = "Report New Bug";
            this.reportBugToolStripMenuItem.Click += new System.EventHandler(this.reportBugToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 634);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(955, 22);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "Ready";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel.Text = "toolStripStatusLabel1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 10);
            this.panel1.TabIndex = 6;
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.tabDashboard);
            this.TabControl1.Controls.Add(this.tabProjectBrowser);
            this.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl1.Location = new System.Drawing.Point(0, 34);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(955, 600);
            this.TabControl1.TabIndex = 7;
            // 
            // tabDashboard
            // 
            this.tabDashboard.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabDashboard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabDashboard.Controls.Add(this.splitContainerDashboard);
            this.tabDashboard.Location = new System.Drawing.Point(4, 22);
            this.tabDashboard.Name = "tabDashboard";
            this.tabDashboard.Padding = new System.Windows.Forms.Padding(3);
            this.tabDashboard.Size = new System.Drawing.Size(947, 574);
            this.tabDashboard.TabIndex = 0;
            this.tabDashboard.Text = "Dashboard";
            // 
            // splitContainerDashboard
            // 
            this.splitContainerDashboard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerDashboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerDashboard.Location = new System.Drawing.Point(3, 3);
            this.splitContainerDashboard.Name = "splitContainerDashboard";
            this.splitContainerDashboard.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerDashboard.Panel1
            // 
            this.splitContainerDashboard.Panel1.Controls.Add(this.dataGridViewBugs);
            // 
            // splitContainerDashboard.Panel2
            // 
            this.splitContainerDashboard.Panel2.Controls.Add(this.dataGridViewActions);
            this.splitContainerDashboard.Panel2.Controls.Add(this.panelBugIdentifier);
            this.splitContainerDashboard.Size = new System.Drawing.Size(939, 566);
            this.splitContainerDashboard.SplitterDistance = 332;
            this.splitContainerDashboard.TabIndex = 2;
            // 
            // dataGridViewBugs
            // 
            this.dataGridViewBugs.AllowUserToAddRows = false;
            this.dataGridViewBugs.AllowUserToDeleteRows = false;
            this.dataGridViewBugs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBugs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewBugs.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewBugs.Name = "dataGridViewBugs";
            this.dataGridViewBugs.ReadOnly = true;
            this.dataGridViewBugs.Size = new System.Drawing.Size(937, 330);
            this.dataGridViewBugs.TabIndex = 1;
            this.dataGridViewBugs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBugs_CellClick);
            this.dataGridViewBugs.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewBugs_CellMouseDoubleClick);
            // 
            // dataGridViewActions
            // 
            this.dataGridViewActions.AllowUserToAddRows = false;
            this.dataGridViewActions.AllowUserToDeleteRows = false;
            this.dataGridViewActions.AllowUserToOrderColumns = true;
            this.dataGridViewActions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewActions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewActions.Location = new System.Drawing.Point(313, 0);
            this.dataGridViewActions.Name = "dataGridViewActions";
            this.dataGridViewActions.ReadOnly = true;
            this.dataGridViewActions.Size = new System.Drawing.Size(624, 228);
            this.dataGridViewActions.TabIndex = 1;
            // 
            // panelBugIdentifier
            // 
            this.panelBugIdentifier.Controls.Add(this.label4);
            this.panelBugIdentifier.Controls.Add(this.label3);
            this.panelBugIdentifier.Controls.Add(this.label2);
            this.panelBugIdentifier.Controls.Add(this.label1);
            this.panelBugIdentifier.Controls.Add(this.textBoxSummary);
            this.panelBugIdentifier.Controls.Add(this.textBoxPriority);
            this.panelBugIdentifier.Controls.Add(this.textBoxStatus);
            this.panelBugIdentifier.Controls.Add(this.textBoxId);
            this.panelBugIdentifier.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelBugIdentifier.Location = new System.Drawing.Point(0, 0);
            this.panelBugIdentifier.Name = "panelBugIdentifier";
            this.panelBugIdentifier.Padding = new System.Windows.Forms.Padding(15);
            this.panelBugIdentifier.Size = new System.Drawing.Size(313, 228);
            this.panelBugIdentifier.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Summary";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Priority";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Status";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID";
            // 
            // textBoxSummary
            // 
            this.textBoxSummary.Location = new System.Drawing.Point(92, 99);
            this.textBoxSummary.Multiline = true;
            this.textBoxSummary.Name = "textBoxSummary";
            this.textBoxSummary.ReadOnly = true;
            this.textBoxSummary.Size = new System.Drawing.Size(203, 111);
            this.textBoxSummary.TabIndex = 3;
            // 
            // textBoxPriority
            // 
            this.textBoxPriority.Location = new System.Drawing.Point(92, 72);
            this.textBoxPriority.Name = "textBoxPriority";
            this.textBoxPriority.ReadOnly = true;
            this.textBoxPriority.Size = new System.Drawing.Size(203, 20);
            this.textBoxPriority.TabIndex = 2;
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Location = new System.Drawing.Point(92, 45);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.ReadOnly = true;
            this.textBoxStatus.Size = new System.Drawing.Size(203, 20);
            this.textBoxStatus.TabIndex = 1;
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(92, 18);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            this.textBoxId.Size = new System.Drawing.Size(203, 20);
            this.textBoxId.TabIndex = 0;
            // 
            // tabProjectBrowser
            // 
            this.tabProjectBrowser.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabProjectBrowser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabProjectBrowser.Controls.Add(this.dataGridViewFileBugs);
            this.tabProjectBrowser.Controls.Add(this.fastColoredTextBoxCodeWindow);
            this.tabProjectBrowser.Controls.Add(this.splitter1);
            this.tabProjectBrowser.Controls.Add(this.treeViewProjectDirectory);
            this.tabProjectBrowser.Location = new System.Drawing.Point(4, 22);
            this.tabProjectBrowser.Name = "tabProjectBrowser";
            this.tabProjectBrowser.Padding = new System.Windows.Forms.Padding(3);
            this.tabProjectBrowser.Size = new System.Drawing.Size(947, 574);
            this.tabProjectBrowser.TabIndex = 1;
            this.tabProjectBrowser.Text = "Project Browser";
            // 
            // dataGridViewFileBugs
            // 
            this.dataGridViewFileBugs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFileBugs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFileBugs.Location = new System.Drawing.Point(562, 3);
            this.dataGridViewFileBugs.Name = "dataGridViewFileBugs";
            this.dataGridViewFileBugs.ReadOnly = true;
            this.dataGridViewFileBugs.Size = new System.Drawing.Size(378, 564);
            this.dataGridViewFileBugs.TabIndex = 3;
            this.dataGridViewFileBugs.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFileBugs_CellDoubleClick);
            // 
            // fastColoredTextBoxCodeWindow
            // 
            this.fastColoredTextBoxCodeWindow.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.fastColoredTextBoxCodeWindow.AutoIndentCharsPatterns = "\n^\\s*[\\w\\.]+(\\s\\w+)?\\s*(?<range>=)\\s*(?<range>[^;]+);\n^\\s*(case|default)\\s*[^:]*(" +
    "?<range>:)\\s*(?<range>[^;]+);\n";
            this.fastColoredTextBoxCodeWindow.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.fastColoredTextBoxCodeWindow.BackBrush = null;
            this.fastColoredTextBoxCodeWindow.BracketsHighlightStrategy = FastColoredTextBoxNS.BracketsHighlightStrategy.Strategy2;
            this.fastColoredTextBoxCodeWindow.CharHeight = 14;
            this.fastColoredTextBoxCodeWindow.CharWidth = 8;
            this.fastColoredTextBoxCodeWindow.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fastColoredTextBoxCodeWindow.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fastColoredTextBoxCodeWindow.Dock = System.Windows.Forms.DockStyle.Left;
            this.fastColoredTextBoxCodeWindow.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.fastColoredTextBoxCodeWindow.IsReplaceMode = false;
            this.fastColoredTextBoxCodeWindow.Language = FastColoredTextBoxNS.Language.CSharp;
            this.fastColoredTextBoxCodeWindow.LeftBracket = '(';
            this.fastColoredTextBoxCodeWindow.LeftBracket2 = '{';
            this.fastColoredTextBoxCodeWindow.Location = new System.Drawing.Point(185, 3);
            this.fastColoredTextBoxCodeWindow.Name = "fastColoredTextBoxCodeWindow";
            this.fastColoredTextBoxCodeWindow.Paddings = new System.Windows.Forms.Padding(0);
            this.fastColoredTextBoxCodeWindow.ReadOnly = true;
            this.fastColoredTextBoxCodeWindow.RightBracket = ')';
            this.fastColoredTextBoxCodeWindow.RightBracket2 = '}';
            this.fastColoredTextBoxCodeWindow.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fastColoredTextBoxCodeWindow.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("fastColoredTextBoxCodeWindow.ServiceColors")));
            this.fastColoredTextBoxCodeWindow.Size = new System.Drawing.Size(377, 564);
            this.fastColoredTextBoxCodeWindow.TabIndex = 2;
            this.fastColoredTextBoxCodeWindow.Zoom = 100;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Silver;
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitter1.Location = new System.Drawing.Point(182, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 564);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // treeViewProjectDirectory
            // 
            this.treeViewProjectDirectory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewProjectDirectory.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeViewProjectDirectory.Location = new System.Drawing.Point(3, 3);
            this.treeViewProjectDirectory.Name = "treeViewProjectDirectory";
            this.treeViewProjectDirectory.Size = new System.Drawing.Size(179, 564);
            this.treeViewProjectDirectory.TabIndex = 0;
            this.treeViewProjectDirectory.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewProjectDirectory_AfterSelect);
            // 
            // katsuDB2DataSet
            // 
            this.katsuDB2DataSet.DataSetName = "KatsuDB2DataSet";
            this.katsuDB2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // katsuDB2DataSetBindingSource
            // 
            this.katsuDB2DataSetBindingSource.DataSource = this.katsuDB2DataSet;
            this.katsuDB2DataSetBindingSource.Position = 0;
            // 
            // FormKatsu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 656);
            this.Controls.Add(this.TabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormKatsu";
            this.Text = "Katsu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.TabControl1.ResumeLayout(false);
            this.tabDashboard.ResumeLayout(false);
            this.splitContainerDashboard.Panel1.ResumeLayout(false);
            this.splitContainerDashboard.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerDashboard)).EndInit();
            this.splitContainerDashboard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBugs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewActions)).EndInit();
            this.panelBugIdentifier.ResumeLayout(false);
            this.panelBugIdentifier.PerformLayout();
            this.tabProjectBrowser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFileBugs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBoxCodeWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.katsuDB2DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.katsuDB2DataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage tabDashboard;
        private System.Windows.Forms.TabPage tabProjectBrowser;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TreeView treeViewProjectDirectory;
        private System.Windows.Forms.ToolStripMenuItem bugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportBugToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridViewBugs;
        private FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBoxCodeWindow;
        private System.Windows.Forms.DataGridView dataGridViewFileBugs;
        private System.Windows.Forms.BindingSource katsuDB2DataSetBindingSource;
        private KatsuDB2DataSet katsuDB2DataSet;
        private System.Windows.Forms.SplitContainer splitContainerDashboard;
        private System.Windows.Forms.DataGridView dataGridViewActions;
        private System.Windows.Forms.Panel panelBugIdentifier;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSummary;
        private System.Windows.Forms.TextBox textBoxPriority;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}