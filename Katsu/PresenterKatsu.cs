﻿using System;
using System.Collections.Generic;
using System.Data;
using LINQtoDataTable;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace Katsu
{
    class PresenterKatsu
    {
        IViewKatsu _IViewKatsu;

        public PresenterKatsu(IViewKatsu iViewKatsu)
        {
            _IViewKatsu = iViewKatsu;
        }

        /// <summary>
        /// Retrieves list of all files associated with Project
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public List<File> getAllProjectFiles(Project project)
        {
            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                List<File> files = dbContext.File.Where(f => f.ProjectId.Equals(project.Id)).ToList();
                return files;
            }
        }

        /// <summary>
        /// Retrieves all bugs associated with file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public List<Bug> getAllFileBugs(File file)
        {
            KatsuDBEntities dbFileBugContext = new KatsuDBEntities();

            List<Bug> bugs = dbFileBugContext.Bug.Where(b => b.FileId == file.Id).ToList();
            return bugs;
        }

        /// <summary>
        /// Retrives all bugs associated with project
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public List<Bug> getAllProjectBugs(Project project)
        {
            KatsuDBEntities dbBugContext = new KatsuDBEntities();

            List<Bug> bugs = dbBugContext.Bug.Where(b => b.ProjectId.Equals(project.Id)).ToList();
            return bugs;

        }

        /// <summary>
        /// Retrieves all actions associated with bug
        /// </summary>
        /// <param name="bug"></param>
        /// <returns></returns>
        public DataTable getAllBugActions(Bug bug)
        {
            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                var query = from a in dbContext.Action
                            join u in dbContext.User on a.UserId equals u.Id
                            where a.BugId == bug.Id
                            select new
                            {
                                Date = a.Timestamp,
                                User = u.FirstName + " " + u.Surname,
                                Action = a.Text,
                                Comments = a.Comments
                            };

                DataTable dt = LINQtoDataTable.CustomLINQtoDataTableMethods.CopyToDataTable(query);
                return dt;
            }
        }

        /// <summary>
        /// Retrieves raw file data from URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string getRawFile(string url)
        {
            using (var client = new HttpClient())
            {
                var response = client.GetStringAsync(url);
                return response.Result;
            }
        }
    }
}
