﻿using Katsu.vcsRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.newProject
{
    class PresenterNewProject
    {
        private IViewNewProject _viewNewProject;
        private IVcsRepository _vcsRepository;

        public PresenterNewProject(IViewNewProject viewNewProject)
        {
            _viewNewProject = viewNewProject;
        }

        /// <summary>
        /// Creates project from VCS repository
        /// </summary>
        /// <param name="projectName">Name of the Project<param>
        /// <param name="vcsUsername">Username of person who owns VCS Repository</param>
        /// <param name="vcsRepository">Name of the VCS Repository</param>
        /// <param name="vcsHost">Name of VCS Host (e.g. Github)</param>
        public void createProject(string projectName, string vcsUsername, string vcsRepository, string vcsHost = "github")
        {
            _viewNewProject.getMainForm().setStatus("Creating Project...");
                        
            if (vcsHost == "github")
            {
                _vcsRepository = new GithubRepository();
            }
            else
            {
                _viewNewProject.showErrorMessage("Unknown VCS host");
                _viewNewProject.getMainForm().resetStatus();
                return;
            }


            if (_vcsRepository.initialiseRepository(vcsUsername, vcsRepository))
            {
                Project project;                

                using (KatsuDBEntities dbContext = new KatsuDBEntities())
                {

                    project = new Project // Create project with provided VCS details
                    {
                        Name = projectName,
                        VcsUsername = vcsUsername,
                        VcsRepository = vcsRepository,
                        VcsHost = vcsHost
                    };
                    dbContext.Project.Add(project);

                    
                    ProjectUserRole projectUserRole = new ProjectUserRole // Add user as project administrator
                    {
                        Role = "Administrator",
                        UserId = _viewNewProject.getUser().Id,
                        ProjectId = project.Id
                    };
                    dbContext.ProjectUserRole.Add(projectUserRole);

                    List<VcsFile> projectDirectory = _vcsRepository.getDirectory();
                    foreach(VcsFile vcsFile in projectDirectory)
                    {
                        File file = new File
                        {
                            Name = vcsFile.name,
                            FilePath = vcsFile.filepath,
                            Url = vcsFile.url,
                            isDirectory = vcsFile.isDirectory,
                            //ProjectId = project.Id
                        };
                        dbContext.File.Add(file);
                    }
                    

                    dbContext.SaveChanges();

                }

                _viewNewProject.getMainForm().init(project);
                _viewNewProject.closeView();

                
            }
            else
            {
                _viewNewProject.showErrorMessage("Project could not be initialised.");
            }

            _viewNewProject.getMainForm().resetStatus();
        }
    }
}
