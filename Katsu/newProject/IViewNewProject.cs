﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.newProject
{
    interface IViewNewProject
    {
        User getUser();

        IViewKatsu getMainForm();

        void closeView();

        void showErrorMessage(string message);
    }
}
