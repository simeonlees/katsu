﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Katsu.vcsRepository;

namespace Katsu.newProject
{
    public partial class FormNewProject : Form, IViewNewProject
    {
        PresenterNewProject _presenterNewProject;
        IViewKatsu _mainForm;
        User _user;

        public FormNewProject(FormKatsu mainForm, User user)
        {
            InitializeComponent();
            _mainForm = mainForm;
            _user = user;
            _presenterNewProject = new PresenterNewProject(this);
        }

        public User getUser()
        {
            return _user;
        }

        public IViewKatsu getMainForm()
        {
            return _mainForm;
        }

        public void closeView()
        {
            this.Close();
        }

        public void showErrorMessage(string message)
        {
            labelNewProjectError.Text = message;
        }

        private void buttonNewProject_Click(object sender, EventArgs e)
        {
            if(textBoxProjectName.Text == "" || textBoxVcsUsername.Text == "" || textBoxVcsRepositoryName.Text == "")
            {
                labelNewProjectError.Text = "Please complete all fields.";
                return;
            }
            _presenterNewProject.createProject(textBoxProjectName.Text, textBoxVcsUsername.Text, textBoxVcsRepositoryName.Text);
        }

        private void buttonCancelNewProject_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
