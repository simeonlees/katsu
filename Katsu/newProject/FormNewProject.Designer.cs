﻿namespace Katsu.newProject
{
    partial class FormNewProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxProjectName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxVcsRepositoryName = new System.Windows.Forms.TextBox();
            this.textBoxVcsUsername = new System.Windows.Forms.TextBox();
            this.buttonCancelNewProject = new System.Windows.Forms.Button();
            this.buttonNewProject = new System.Windows.Forms.Button();
            this.labelNewProjectError = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxProjectName);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxVcsRepositoryName);
            this.panel1.Controls.Add(this.textBoxVcsUsername);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(326, 200);
            this.panel1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Project Name *";
            // 
            // textBoxProjectName
            // 
            this.textBoxProjectName.Location = new System.Drawing.Point(123, 25);
            this.textBoxProjectName.Name = "textBoxProjectName";
            this.textBoxProjectName.Size = new System.Drawing.Size(200, 20);
            this.textBoxProjectName.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton1);
            this.panel2.Location = new System.Drawing.Point(123, 145);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 42);
            this.panel2.TabIndex = 6;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(56, 17);
            this.radioButton1.TabIndex = 3;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Github";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "VCS Host";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "VCS Repository Name *";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "VCS Username *";
            // 
            // textBoxVcsRepositoryName
            // 
            this.textBoxVcsRepositoryName.Location = new System.Drawing.Point(123, 106);
            this.textBoxVcsRepositoryName.Name = "textBoxVcsRepositoryName";
            this.textBoxVcsRepositoryName.Size = new System.Drawing.Size(200, 20);
            this.textBoxVcsRepositoryName.TabIndex = 2;
            // 
            // textBoxVcsUsername
            // 
            this.textBoxVcsUsername.Location = new System.Drawing.Point(123, 66);
            this.textBoxVcsUsername.Name = "textBoxVcsUsername";
            this.textBoxVcsUsername.Size = new System.Drawing.Size(200, 20);
            this.textBoxVcsUsername.TabIndex = 1;
            // 
            // buttonCancelNewProject
            // 
            this.buttonCancelNewProject.Location = new System.Drawing.Point(248, 243);
            this.buttonCancelNewProject.Name = "buttonCancelNewProject";
            this.buttonCancelNewProject.Size = new System.Drawing.Size(90, 28);
            this.buttonCancelNewProject.TabIndex = 5;
            this.buttonCancelNewProject.Text = "Cancel";
            this.buttonCancelNewProject.UseVisualStyleBackColor = true;
            this.buttonCancelNewProject.Click += new System.EventHandler(this.buttonCancelNewProject_Click);
            // 
            // buttonNewProject
            // 
            this.buttonNewProject.Location = new System.Drawing.Point(152, 243);
            this.buttonNewProject.Name = "buttonNewProject";
            this.buttonNewProject.Size = new System.Drawing.Size(90, 28);
            this.buttonNewProject.TabIndex = 4;
            this.buttonNewProject.Text = "Ok";
            this.buttonNewProject.UseVisualStyleBackColor = true;
            this.buttonNewProject.Click += new System.EventHandler(this.buttonNewProject_Click);
            // 
            // labelNewProjectError
            // 
            this.labelNewProjectError.AutoSize = true;
            this.labelNewProjectError.ForeColor = System.Drawing.Color.Red;
            this.labelNewProjectError.Location = new System.Drawing.Point(15, 215);
            this.labelNewProjectError.Name = "labelNewProjectError";
            this.labelNewProjectError.Size = new System.Drawing.Size(0, 13);
            this.labelNewProjectError.TabIndex = 3;
            // 
            // FormNewProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 283);
            this.Controls.Add(this.labelNewProjectError);
            this.Controls.Add(this.buttonNewProject);
            this.Controls.Add(this.buttonCancelNewProject);
            this.Controls.Add(this.panel1);
            this.Name = "FormNewProject";
            this.Text = "New Project";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxVcsRepositoryName;
        private System.Windows.Forms.TextBox textBoxVcsUsername;
        private System.Windows.Forms.Button buttonCancelNewProject;
        private System.Windows.Forms.Button buttonNewProject;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxProjectName;
        private System.Windows.Forms.Label labelNewProjectError;
    }
}