﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.openProject
{
    interface IViewOpenProject
    {
        User getUser();

        IViewKatsu getMainForm();

        void closeView();

        void populateProjectList(List<Project> projectList);

        void showErrorMessage(string message);
        
    }
}
