﻿namespace Katsu.openProject
{
    partial class FormOpenProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancelOpenProject = new System.Windows.Forms.Button();
            this.buttonOpenProject = new System.Windows.Forms.Button();
            this.listBoxProjects = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // buttonCancelOpenProject
            // 
            this.buttonCancelOpenProject.Location = new System.Drawing.Point(182, 221);
            this.buttonCancelOpenProject.Name = "buttonCancelOpenProject";
            this.buttonCancelOpenProject.Size = new System.Drawing.Size(90, 28);
            this.buttonCancelOpenProject.TabIndex = 2;
            this.buttonCancelOpenProject.Text = "Cancel";
            this.buttonCancelOpenProject.UseVisualStyleBackColor = true;
            this.buttonCancelOpenProject.Click += new System.EventHandler(this.buttonCancelOpenProject_Click);
            // 
            // buttonOpenProject
            // 
            this.buttonOpenProject.Location = new System.Drawing.Point(86, 221);
            this.buttonOpenProject.Name = "buttonOpenProject";
            this.buttonOpenProject.Size = new System.Drawing.Size(90, 28);
            this.buttonOpenProject.TabIndex = 1;
            this.buttonOpenProject.Text = "Ok";
            this.buttonOpenProject.UseVisualStyleBackColor = true;
            this.buttonOpenProject.Click += new System.EventHandler(this.buttonOpenProject_Click);
            // 
            // listBoxProjects
            // 
            this.listBoxProjects.FormattingEnabled = true;
            this.listBoxProjects.Location = new System.Drawing.Point(13, 13);
            this.listBoxProjects.Name = "listBoxProjects";
            this.listBoxProjects.Size = new System.Drawing.Size(259, 199);
            this.listBoxProjects.TabIndex = 0;
            // 
            // FormOpenProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.listBoxProjects);
            this.Controls.Add(this.buttonOpenProject);
            this.Controls.Add(this.buttonCancelOpenProject);
            this.Name = "FormOpenProject";
            this.Text = "Open Project";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancelOpenProject;
        private System.Windows.Forms.Button buttonOpenProject;
        private System.Windows.Forms.ListBox listBoxProjects;
    }
}