﻿using Katsu.vcsRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.openProject
{
    class PresenterOpenProject
    {
        private IViewOpenProject _viewOpenProject;
        private List<Project> _projectList;
        private User _user;

        public PresenterOpenProject(IViewOpenProject viewOpenProject)
        {
            _viewOpenProject = viewOpenProject;
            _user = _viewOpenProject.getUser();
            _projectList = new List<Project>();

            // Retrieves all projects which user has a user role for
            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                var query = from p in dbContext.Project
                            from r in p.ProjectUserRole
                            where r.UserId == _user.Id
                            select p;

                foreach (Project result in query)
                {
                    _projectList.Add(result);
                }
            }

            _viewOpenProject.populateProjectList(_projectList);
            
        }

        /// <summary>
        /// Loads Project and initialises main form
        /// </summary>
        /// <param name="id">Project ID</param>
        public void openProject(int id)
        {
            Project project;

            _viewOpenProject.getMainForm().setStatus("Opening Project...");

            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                project = dbContext.Project.FirstOrDefault(p => p.Id.Equals(id));
            }

            if (project != null)
            {
                _viewOpenProject.getMainForm().init(project);
                _viewOpenProject.closeView();
            }
            else
            {
                _viewOpenProject.showErrorMessage("Could not load project");
            }

            _viewOpenProject.getMainForm().resetStatus();


            
        }

    }
}
