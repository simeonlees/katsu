﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Katsu.openProject
{
    public partial class FormOpenProject : Form, IViewOpenProject
    {
        PresenterOpenProject _presenterOpenProject;
        IViewKatsu _mainForm;
        User _user;
        List<Project> _projectList;

        public FormOpenProject(IViewKatsu mainForm, User user)
        {
            InitializeComponent();
            _mainForm = mainForm;
            _user = user;
            _presenterOpenProject = new PresenterOpenProject(this);
        }

        public void populateProjectList(List<Project> projectList)
        {
            _projectList = projectList;

            listBoxProjects.DataSource = _projectList;
            listBoxProjects.DisplayMember = "Name";
            listBoxProjects.ValueMember = "Id";
        }

        public User getUser()
        {
            return _user;
        }

        public IViewKatsu getMainForm()
        {
            return _mainForm;
        }

        public void showErrorMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void closeView()
        {
            this.Close();
        }

        private void buttonCancelOpenProject_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOpenProject_Click(object sender, EventArgs e)
        {
            int value = (int)listBoxProjects.SelectedValue;
            _presenterOpenProject.openProject(value);
        }
    }
}
