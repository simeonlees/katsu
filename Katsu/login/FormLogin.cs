﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Katsu.login
{
    public partial class FormLogin : Form, IViewLogin
    {
        PresenterLogin _presenterLogin;

        public FormLogin()
        {
            InitializeComponent();
            _presenterLogin = new PresenterLogin(this);
        }

        public void setLoginStatus(string message)
        {
            labelFormErrors.Text = message;
        }

        public void hideView()
        {
            this.Hide();
        }

        public void closeView()
        {
            this.Close();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                _presenterLogin.login(textBoxUsername.Text, textBoxPassword.Text, dbContext);
            }
        }
    }
}
