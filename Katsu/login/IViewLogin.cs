﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.login
{
    public interface IViewLogin
    {
        void setLoginStatus(string message);

        void hideView();

        void closeView();
    }
}
