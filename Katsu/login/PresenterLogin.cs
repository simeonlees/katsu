﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.login
{
    public class PresenterLogin
    {
        private IViewLogin _viewLogin;
        private User _user;

        public PresenterLogin(IViewLogin viewLogin)
        {
            _viewLogin = viewLogin;
        }

        /// <summary>
        /// Calls GetUser with provided details, and if successful loads up main form
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public bool login(string username, string password, KatsuDBEntities dbContext)
        {
            _user = getUser(username, password, dbContext);

            if (_user != null)
            {
                _viewLogin.hideView();
                FormKatsu katsu = new FormKatsu(_user);
                katsu.FormClosed += (s, args) => _viewLogin.closeView(); // Adds delegate so that login form is closed when main window is closed
                katsu.Show();
            }
            else
            {
                _viewLogin.setLoginStatus("Username or password incorrect");
            }
            

            return true;
        }

        /// <summary>
        /// Retrieves first matching row in which Username and Password both match provided
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public User getUser(string username, string password, KatsuDBEntities dbContext)
        {            
            return dbContext.User.FirstOrDefault(r => r.Username.Equals(username) && r.Password.Equals(password));
        }
    }
}
