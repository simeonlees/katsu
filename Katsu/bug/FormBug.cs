﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Katsu.bug;

namespace Katsu.bug
{
    public partial class FormBug : Form, IViewBug
    {
        IViewKatsu _mainForm;
        Bug _bug;
        PresenterBug _presenterBug;
        List<File> _fileList;

        public FormBug(IViewKatsu formKatsu, Bug bug = null)
        {
            InitializeComponent();
            if (!formKatsu.isInitialized())
            {
                this.Close();
                return;
            }
            _mainForm = formKatsu;
            _bug = bug;
            _presenterBug = new PresenterBug(this, formKatsu, _mainForm.getProjectId());

            int tempFileId = 0;
            try
            {
                tempFileId = bug.FileId ?? 0;
            } catch {}
            

            if (bug != null) // Update bug, initialise form with info
            {
                if (bug.FileId > 0)
                {
                    File file = _fileList.Find(f => f.Id == bug.FileId);
                    comboBoxFile.SelectedIndex = comboBoxFile.FindString(file.FilePath);
                }

                this.Text = "Update Bug";
                buttonOk.Text = "Update";
                textBoxSummary.Text = bug.Summary;
                textBoxSymptoms.Text = bug.Symptoms;
                textBoxStepsToRecreate.Text = bug.StepsToRecreate;
                textBoxTags.Text = bug.Tags;
                numericLineNumLow.Value = bug.LineNumLow ?? 0;
                numericLineNumHigh.Value = bug.LineNumHigh ?? 0;
                comboBoxStatus.SelectedItem = bug.Status;
                comboBoxPriority.SelectedItem = bug.Priority;
            }
            else // New bug
            {
                comboBoxStatus.SelectedIndex = 0; // New
                comboBoxPriority.SelectedIndex = 1; // Medium
            }
        }

        public void closeView()
        {
            this.Close();
        }

        public int getUserId()
        {
            return _mainForm.getUserId();
        }

        public void showErrorMessage(string message)
        {
            // Show message NEEDS IMPLEMENTING
        }

        public void populateFilesList(List<File> fileList)
        {
            _fileList = fileList;
            comboBoxFile.DataSource = fileList;
            comboBoxFile.DisplayMember = "FilePath";
            comboBoxFile.ValueMember = "Id";
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (textBoxSummary.Text == "" || textBoxSymptoms.Text == "")
            {
                labelErrorMessage.Text = "Please complete all required fields (*)";
                return;
            }

            int value = (int)comboBoxFile.SelectedValue;

            int? fileIdInt;

            if (value == 0)
            {
                fileIdInt = null;
            }
            else
            {
                fileIdInt = value;
            }

            if (_bug != null)
            {
                _presenterBug.updateBug(
                    _bug.Id,
                    textBoxSummary.Text, //string summary,
                    textBoxSymptoms.Text, //string symptoms,
                    comboBoxStatus.Text, //string Status,
                    comboBoxPriority.Text, //string Priority,
                    _mainForm.getProjectId(), //int projectId,
                    _mainForm.getUserId(), //int userId,
                    textBoxStepsToRecreate.Text, //string stepsToRecreate = null,
                    textBoxTags.Text,
                    textBoxComments.Text,
                    fileIdInt, //string tags = null,
                    (int?)numericLineNumLow.Value, //int ? lineNumLow = null,
                    (int?)numericLineNumHigh.Value //int ? lineNumHigh = null
                );
            }
            else
            {
                

                _presenterBug.reportBug(
                    textBoxSummary.Text, //string summary,
                    textBoxSymptoms.Text, //string symptoms,
                    comboBoxStatus.Text, //string Status,
                    comboBoxPriority.Text, //string Priority,
                    _mainForm.getProjectId(), //int projectId,
                    _mainForm.getUserId(), //int userId,
                    textBoxStepsToRecreate.Text, //string stepsToRecreate = null,
                    textBoxTags.Text, 
                    textBoxComments.Text,
                    fileIdInt, //string tags = null,
                    (int?)numericLineNumLow.Value, //int ? lineNumLow = null,
                    (int?)numericLineNumHigh.Value //int ? lineNumHigh = null
                );
            }
        }
    }
}
