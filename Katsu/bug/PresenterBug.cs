﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.bug
{
    class PresenterBug
    {
        IViewBug _iViewBug;
        IViewKatsu _iViewKatsu;
        List<File> _projectFiles;
        int _projectId;

        public PresenterBug(IViewBug iViewBug, IViewKatsu iViewKatsu, int projectId)
        {
            _iViewBug = iViewBug;
            _iViewKatsu = iViewKatsu;
            _projectId = projectId;
            _projectFiles = new List<File>();

            // Initialises list of project files
            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                var query = from p in dbContext.Project
                            from f in p.File
                            where p.Id == _projectId
                            select f;

                foreach (File result in query)
                {
                    _projectFiles.Add(result);
                }

                // Adds "None" option at start of file list
                File noFile = new File{FilePath = "(None)"};

                _projectFiles.Insert(0, noFile);
            }

            _iViewBug.populateFilesList(_projectFiles);
        }

        /// <summary>
        /// Updates Bug with provided information
        /// </summary>
        /// <param name="bugId"></param>
        /// <param name="summary"></param>
        /// <param name="symptoms"></param>
        /// <param name="Status"></param>
        /// <param name="Priority"></param>
        /// <param name="projectId"></param>
        /// <param name="userId"></param>
        /// <param name="stepsToRecreate"></param>
        /// <param name="tags"></param>
        /// <param name="comments"></param>
        /// <param name="fileId"></param>
        /// <param name="lineNumLow"></param>
        /// <param name="lineNumHigh"></param>
        public void updateBug(
            int bugId,
            string summary,
            string symptoms,
            string Status,
            string Priority,
            int projectId,
            int userId,
            string stepsToRecreate = null,
            string tags = null,
            string comments = null,
            int? fileId = null,
            int? lineNumLow = null,
            int? lineNumHigh = null
        )
        {

            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                Bug bug = dbContext.Bug.FirstOrDefault(b => b.Id == bugId);

                if (bug == null)
                {
                    return;
                }

                _iViewKatsu.selectBug(bugId);

                List<string> changedFields = new List<string>();
                string text = "";

                if(bug.Summary != summary){ changedFields.Add("Summary"); }
                if(bug.Symptoms != symptoms) { changedFields.Add("Symptoms"); }
                if(bug.Priority != Priority){ changedFields.Add("Priority"); }
                if(bug.StepsToRecreate != stepsToRecreate){ changedFields.Add("Steps to recreate"); }
                if(bug.Tags != tags){ changedFields.Add("Tags"); }
                if(bug.FileId != fileId){ changedFields.Add("File"); }
                if(bug.LineNumHigh != lineNumHigh){ changedFields.Add("Upper Line number"); }
                if(bug.LineNumLow != lineNumLow){ changedFields.Add("Lower Line number"); }


                

                if (changedFields.Count > 0)
                {
                    text += "Updated the " + String.Join(", ", changedFields.ToArray());
                }

                if (bug.Status != Status)
                {
                    string statusText = "Changed the status from " + bug.Status + " to " + Status;
                    statusText += changedFields.Count > 0 ? " and " : "";
                    text = statusText + text;
                    changedFields.Add("Status");
                }


                bug.Summary = summary;
                bug.Symptoms = symptoms;
                bug.Status = Status;
                bug.Priority = Priority;
                bug.StepsToRecreate = stepsToRecreate;
                bug.Tags = tags;
                bug.FileId = fileId;
                bug.LineNumLow = lineNumLow;
                bug.LineNumHigh = lineNumHigh;
                bug.DateRaised = DateTime.Now;

                if(changedFields.Count > 0)
                {
                    Action action = new Action
                    {
                        Text = text,
                        UserId = _iViewBug.getUserId(),
                        BugId = bug.Id,
                        Timestamp = DateTime.Now,
                        Comments = comments
                    };

                    dbContext.Action.Add(action);
                    dbContext.SaveChanges();

                    _iViewKatsu.refresh();
                    _iViewKatsu.selectBug(bug.Id);
                }
                
            }

            _iViewBug.closeView();

        }

        /// <summary>
        /// Reports bug with provided information
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="symptoms"></param>
        /// <param name="Status"></param>
        /// <param name="Priority"></param>
        /// <param name="projectId"></param>
        /// <param name="userId"></param>
        /// <param name="stepsToRecreate"></param>
        /// <param name="tags"></param>
        /// <param name="comments"></param>
        /// <param name="fileId"></param>
        /// <param name="lineNumLow"></param>
        /// <param name="lineNumHigh"></param>
        public void reportBug(
            string summary,
            string symptoms,
            string Status,
            string Priority,
            int projectId,
            int userId,
            string stepsToRecreate = null,
            string tags = null,
            string comments = null,
            int? fileId = null,
            int? lineNumLow = null,
            int? lineNumHigh = null
        )
        {

            using (KatsuDBEntities dbContext = new KatsuDBEntities())
            {
                Bug bug = new Bug
                {
                    Summary = summary,
                    Symptoms = symptoms,
                    Status = Status,
                    Priority = Priority,
                    ProjectId = _projectId,
                    RaisedByUserId = _iViewBug.getUserId(),
                    StepsToRecreate = stepsToRecreate,
                    Tags = tags,
                    FileId = fileId,
                    LineNumLow = lineNumLow,
                    LineNumHigh = lineNumHigh,
                    DateRaised = DateTime.Now
                
                };

                Action action = new Action
                {
                    Text = "Reported bug with " + Priority + " priority",
                    UserId = _iViewBug.getUserId(),
                    BugId = bug.Id,
                    Timestamp = DateTime.Now,
                    Comments = comments
                };

                dbContext.Bug.Add(bug);
                dbContext.Action.Add(action);
                dbContext.SaveChanges();

                _iViewKatsu.refresh();
                _iViewKatsu.selectBug(bug.Id);
            }

            _iViewBug.closeView();
            
        }

    }
}
