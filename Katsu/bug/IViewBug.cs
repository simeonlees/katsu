﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.bug
{
    interface IViewBug
    {
        void showErrorMessage(string message);

        void populateFilesList(List<File> fileList);

        int getUserId();

        void closeView();
    }
}
