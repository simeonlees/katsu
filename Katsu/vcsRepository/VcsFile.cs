﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.vcsRepository
{
    public struct VcsFile
    {
        public string name;
        public string filepath;
        public string url;
        public bool isDirectory;
    }
}
