﻿using Katsu.vcsRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu
{
    interface IVcsRepository
    {
        /// <summary>
        /// Initialises the iVcsRepository object. This creates a URL from the provided username and repository, calls the URL then deserialises the returned JSON.
        /// </summary>
        /// <param name="username">The VCS username for the repository.</param>
        /// <param name="repository">The VCS Repository name.</param>
        bool initialiseRepository(string username, string repository);

        List<VcsFile> getDirectory();
    }
}
