﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katsu.vcsRepository
{
    public class GithubJsonNodeObject
    {
        public string path { get; set; }
        public string mode { get; set; }
        public string type { get; set; }
        public string sha { get; set; }
        public int size { get; set; }
        public string url { get; set; }
    }

    public class GithubJsonObject
    {
        public string sha { get; set; }
        public string url { get; set; }
        public List<GithubJsonNodeObject> tree { get; set; }
        public bool truncated { get; set; }
    }
}
