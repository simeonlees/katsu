﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Katsu.vcsRepository;

namespace Katsu.vcsRepository
{
    public class GithubRepository : IVcsRepository
    {
        string repositoryUrl;
        string _username;
        string _repositoryName;
        GithubJsonObject githubJsonObj;

        /// <summary>
        /// Attempts to create a repository with the provided Github details.
        /// </summary>
        /// <param name="username">VCS username</param>
        /// <param name="repository">VCS repository</param>
        /// <returns>Bool indicating success</returns>
        public bool initialiseRepository(string username, string repository)
        {
            repositoryUrl = "https://api.github.com/repos/"+username+"/"+repository+"/git/trees/master?recursive=1";
            _username = username;
            _repositoryName = repository;

            try
            {
                string json = getGithubData(repositoryUrl);
                githubJsonObj = JsonConvert.DeserializeObject<GithubJsonObject>(json);
                return true;
            }
            catch
            {
                repositoryUrl = null;
                githubJsonObj = null;
                return false;
            }
            
        }

        /// <summary>
        /// Retrieves data from a provided URL
        /// </summary>
        /// <param name="url">URL to send GET request</param>
        /// <returns></returns>
        public string getGithubData(string url)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                var response = client.GetStringAsync(url);
                return response.Result;
            }
        }

        /// <summary>
        /// Creates directory from provided list of files
        /// </summary>
        /// <returns></returns>
        public List<VcsFile> getDirectory()
        {
            List<VcsFile> directory = new List<VcsFile>();
            bool isDir;
            string fileName;
            string fileUrl;

            foreach(GithubJsonNodeObject file in githubJsonObj.tree)
            {
                fileName = file.path;
                int x = fileName.LastIndexOf("/");
                if (x != -1) { fileName = fileName.Substring(x+1); } // Creates substring from after last "/" if in string

                if (file.type == "tree")
                {
                    isDir = true;
                    fileUrl = null;
                }
                else
                {
                    isDir = false;
                    fileUrl = "https://raw.githubusercontent.com/" + _username + "/" + _repositoryName + "/master/" + file.path;
                }

                directory.Add(new VcsFile {
                    name = fileName,
                    filepath = file.path,
                    url = fileUrl,
                    isDirectory = isDir
                });
            }

            return directory;
        }
    }
}
